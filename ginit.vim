if exists('g:GtkGuiLoaded')
  
call rpcnotify(1, 'Gui', 'Font', 'DejaVu Sans Mono 13')
call rpcnotify(1, 'Gui', 'Option', 'Tabline', 0)
NVIM_GTK_NO_HEADERBAR=1
NVIM_GTK_PREFER_DARK_THEME=1

endif
