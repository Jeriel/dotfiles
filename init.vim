"Plugins

call plug#begin()


function! DoRemote(arg)
  UpdateRemotePlugins
endfunction


Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

Plug 'junegunn/fzf'
Plug 'Shougo/deoplete.nvim', { 'do': function('DoRemote') }
Plug 'scrooloose/nerdtree'
Plug 'ervandew/supertab'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'w0rp/ale'
"Plug 'joshdick/onedark.vim'
"Plug 'rakr/vim-one'
Plug 'monkeyTux/vim-one'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'jiangmiao/auto-pairs'
"Plug 'valloric/youcompleteme'

Plug 'airblade/vim-gitgutter'
"Plug 'vim-syntastic/syntastic'
"Plug 'cespare/vim-toml'
Plug 'ap/vim-css-color'
Plug 'sheerun/vim-polyglot'
Plug 'terryma/vim-multiple-cursors'
Plug 'neomake/neomake', { 'for': 'rust' }
Plug 'scrooloose/nerdcommenter'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
Plug 'ap/vim-css-color'
Plug 'leafgarland/typescript-vim'
Plug 'hail2u/vim-css3-syntax'
Plug 'quramy/tsuquyomi'
Plug 'prettier/vim-prettier', { 'do': 'yarn install' }
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'amix/vim-zenroom2'
Plug 'mattn/emmet-vim'
Plug 'severin-lemaignan/vim-minimap'
Plug 'DougBeney/pickachu'
Plug 'ryanoasis/vim-devicons'
Plug 'majutsushi/tagbar'
Plug 'andreyorst/SimpleWorkspaces.vim'
Plug 'tpope/vim-surround'

" .nvimrc
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set guifont=Source\ Code\ Pro\ Light:h20
autocmd BufNewFile,BufReadPost *.md set filetype=markdown
set fillchars+=vert:\|

map gn :bn<cr>
map gp :bp<cr>
map gd :bd<cr>

" Language plugins

" Rust Plugins
if executable('rustc')
  Plug 'rust-lang/rust.vim', { 'for': 'rust' }
  Plug 'racer-rust/vim-racer', { 'for': 'rust' }
endif

call plug#end()

" Deoplete
let g:deoplete#enable_at_startup = 1

let g:deoplete#sources#rust#racer_binary='~/.cargo/bin/racer'

let g:deoplete#sources#rust#rust_source_path='~/.config/nvim/rust-src'

let g:deoplete#sources#rust#show_duplicates=1

let g:deoplete#sources#rust#disable_keymap=1

let g:deoplete#sources#rust#documentation_max_height=20


set hidden
let g:racer_cmd = "~/.cargo/bin/racer"
let g:racer_experimental_completer = 1
au FileType rust nmap <leader>rx <Plug>(rust-doc)
au FileType rust nmap <leader>rd <Plug>(rust-def)
au FileType rust nmap <leader>rs <Plug>(rust-def-split)

" ctrl z


" Neomake
" Gross hack to stop Neomake running when exitting because it creates a zombie cargo check process
" which holds the lock and never exits. But then, if you only have QuitPre, closing one pane will
" disable neomake, so BufEnter reenables when you enter another buffer.
let s:quitting = 0
au QuitPre *.rs let s:quitting = 1
au BufEnter *.rs let s:quitting = 0
au BufWritePost *.rs if ! s:quitting | Neomake | else | echom "Neomake disabled"| endif
au QuitPre *.ts let s:quitting = 1
au BufEnter *.ts let s:quitting = 0
au BufWritePost *.ts if ! s:quitting | Neomake | else | echom "Neomake disabled"| endif
let g:neomake_warning_sign = {'text': '?'}

" ALE
let g:airline#extensions#ale#enabled = 1
"let g:ale_linters = {
"\   'rust': ['rls', 'cargo', 'rustc']
"\}
let g:ale_rust_rls_toolchain = 'nightly'
let g:ale_completion_enabled = 1
let g:ale_rust_cargo_use_check = 1
let g:ale_lint_on_text_changed = 1 "dont lint on text change
let g:ale_lint_on_save = 1 "lint on save
let g:ale_sign_warning = ''
let g:ale_sign_error = ''
let g:ale_echo_msg_format = '[%severity%]: %s [%linter%]'
let g:ale_statusline_format = ['E:%s', 'W:%s', 'OK']
highlight ALEErrorSign ctermbg=NONE ctermfg=red
highlight ALEWarningSign ctermbg=NONE ctermfg=yellow
let g:ale_lint_delay = 800
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
let g:ale_linters = {
	\ 'javascript': ['eslint'],
	\ 'python': ['pyls'],
	\ 'php': ['langserver'],
	\ 'rust': ['rls'],
	\ 'elixir': ['elixir-ls'],
	\ 'c': ['clangd']
	\}


" Airline Theme
let g:airline_theme='one'
let g:airline#extensions#tabline#enabled =1
set t_Co=256

" Theme

set termguicolors
syntax enable
colorscheme one
set background=dark " for the dark version
" set background=light " for the light version

let g:airline_powerline_fonts = 1

" airline theme
"let g:airline_theme='onedark'
"let g:onedark_termcolors=256

set number

set relativenumber


set inccommand=split
set expandtab
set shiftwidth=2

" history
set history=700

" Vim recommended
if has('autocmd')
  filetype plugin indent on
endif
if has('syntax') && !exists('g:syntax_on')
  syntax enable
endif
set synmaxcol=9999
set hidden " Allows you to switch buffers without saving current
set wildmenu "tab completion
set wildmode=longest:full,full " First tab brings up options, second tab cycles
set encoding=utf8

" Movement
let mapleader = " "
set tm=2000
noremap ,, ,

" Enable mouse support
set mouse=a


" Always show current position
set ruler

" Better searching
set incsearch
set ignorecase
set smartcase
set wrapscan "wraps around end of file
" Redraw screen and clear highlighting
nnoremap <Leader>r :nohl<CR><C-L>

" tabs
set expandtab
set smarttab


" Show matching bracket
set showmatch
set matchtime=2
set shiftwidth=2
set tabstop=2

" Close nerdtree after a file is selected
let NERDTreeQuitOnOpen = 1

function! IsNERDTreeOpen()
  return exists("t:NERDTreeBufName") && (bufwinnr(t:NERDTreeBufName) != -1)
endfunction

function! ToggleFindNerd()
  if IsNERDTreeOpen()
    exec ':NERDTreeToggle'
  else
    exec ':NERDTreeFind'
  endif
endfunction


" CtrlP using ripgrep
if executable('rg')
  set grepprg=rg\ 
  let g:ctrlp_user_command = 'rg %s --files --glob ""'
  let g:ctrlp_use_caching = 0
endif

" If nerd tree is closed, find current file, if open, close it
nmap <silent> <leader>f <ESC>:call ToggleFindNerd()<CR>
nmap <silent> <leader>F <ESC>:NERDTreeToggle<CR>

set statusline=%f\ %h%w%m%r\ %=%(%l,%c%V\ %=\ %P%)

" Ale syntax checking
let g:ale_rust_cargo_use_check = 1
" use Ctrl-k and Ctrl-j to jump up and down between errors
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

" rustfmt on save
let g:rustfmt_autosave = 1

"Emmet
let g:user_emmet_settings = {
  \  'javascript.jsx' : {
    \      'extends' : 'jsx',
    \  },
  \}


set foldmethod=indent   
set foldnestmax=10
set nofoldenable
set foldlevel=2

nnoremap <Leader>] :YcmCompleter GoTo<CR>

" a basic set up for LanguageClient-Neovim

" << LSP >> {{{

let g:LanguageClient_autoStart = 0
nnoremap <leader>lcs :LanguageClientStart<CR>

" if you want it to turn on automatically
" let g:LanguageClient_autoStart = 1

let g:LanguageClient_serverCommands = {
    \ 'python': ['pyls'],
    \ 'rust': ['rustup', 'run', 'nightly', 'rls', 'beta'],
    \ 'javascript': ['javascript-typescript-stdio'],
    \ 'go': ['go-langserver'] }

noremap <silent> H :call LanguageClient_textDocument_hover()<CR>
noremap <silent> Z :call LanguageClient_textDocument_definition()<CR>
noremap <silent> R :call LanguageClient_textDocument_rename()<CR>
noremap <silent> S :call LanugageClient_textDocument_documentSymbol()<CR>
" }}}CR
" Moving lines
function! s:swap_lines(n1, n2)
    let line1 = getline(a:n1)
    let line2 = getline(a:n2)
    call setline(a:n1, line2)
    call setline(a:n2, line1)
endfunction

function! s:swap_up()
    let n = line('.')
    if n == 1
        return
    endif

    call s:swap_lines(n, n - 1)
    exec n - 1
endfunction

function! s:swap_down()
    let n = line('.')
    if n == line('$')
        return
    endif

    call s:swap_lines(n, n + 1)
    exec n + 1
endfunction

noremap <silent> <c-s-up> :call <SID>swap_up()<CR>
noremap <silent> <c-s-down> :call <SID>swap_down()<CR>

" Pickachu Map
map <A-c> :Pickachu<CR>

" Tagbar
nmap <F8> :TagbarToggle<CR>

" Split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Select all
map <C-a> <esc>ggVG<CR>

" Reactjs
let g:jsx_ext_required = 0 " Allow JSX in normal JS files